<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use App\Orders;
use App\Products;
use App\Shopsetting;
use Spatie\Valuestore\Valuestore;
use Illuminate\Http\Request;

class ImportOrdersController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function Index()
    {
        return view('import');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function ImportOrders()
    {

        $shops = Shopsetting::all();

        foreach ($shops as $shop) {
            $config = Config::get('database.connections.mysql_import');

            $config['host'] = $shop->host;
            $config['port'] = $shop->port;
            $config['database'] = $shop->db_name;
            $config['username'] = $shop->user_name;
            $config['password'] = $shop->password;

            config()->set('database.connections.mysql_import', $config);

            if ($shop->type == 'opencart_2') {
                $this->import_opencart2($shop->code, $shop->prefix);
            }

            if ($shop->type == 'opencart_3') {
                $this->import_opencart3($shop->code, $shop->prefix);
            }
        }

        $settings = Valuestore::make(storage_path('app/data.json'));
        $settings->put('update_orders', date("Y-m-d H:i:s"));

        return array('status' => 'ok', 'data' => '');
    }

    private function import_opencart2($code2, $prefix2)
    {

        $sql = 'select *, os.name as os_name from ' . $prefix2 . '_order o left join ' . $prefix2 . '_order_status os on (os.order_status_id = o.order_status_id ) order by o.order_id desc limit 10';

        DB::reconnect('mysql_import');
        $rows = DB::connection('mysql_import')->select($sql);
        foreach ($rows as $row) {
            $order = Orders::firstOrCreate(array('codefkshop' => $code2, 'number_order' => $row->order_id));

            $order->date_order = $row->date_added;
            $order->status_order = $row->os_name;

            $order->total_order = $row->total;
            $order->total_est_order = $row->total * 0.7;
            $order->comment_order = $row->comment;

            if ($row->shipping_delivery_date > '1990.01.01') {
                $order->date_delivery = $row->shipping_delivery_date;
            }
            //$order->from_delivery = $row->;
            $order->to_delivery = $row->shipping_city;
            $order->address_delivery = $row->shipping_address_1;
            $order->time_delivery = $row->shipping_time_from . ' - ' . $row->shipping_time_to;
            $order->pickup_delivery = $row->shipping_address_2;

            $order->fio_delivery = $row->firstname . ' ' . $row->lastname;
            $order->phone_delivery = $row->telephone;

            $sql_product = 'select * from ' . $prefix2 . '_order_product where order_id='.$row->order_id;
            DB::reconnect('mysql_import');
            $product_rows = DB::connection('mysql_import')->select($sql_product);
            foreach ($product_rows as $product_row) {
                $product = Products::firstOrCreate(array('idfkorder' => $order->id, 'id_order_product' => $product_row->order_product_id));

                $product->title = $product_row->name;
                $product->model = $product_row->model;
                $product->price = $product_row->price;
                $product->quantity = $product_row->quantity;
                $product->total = $product_row->total;

                $product->save();
            }

            unset($product_rows);

            $order->save();

        }
        unset($rows);

    }

    private function import_opencart3($code3, $prefix3)
    {

        $sql = 'select *, os.name as os_name from ' . $prefix3 . '_order o left join ' . $prefix3 . '_order_status os on (os.order_status_id = o.order_status_id ) order by o.order_id desc limit 10';

        DB::reconnect('mysql_import');
        $rows = DB::connection('mysql_import')->select($sql);
        foreach ($rows as $row) {
            $order = Orders::firstOrCreate(array('codefkshop' => $code3, 'number_order' => $row->order_id));

            $order->date_order = $row->date_added;
            $order->status_order = $row->os_name;

            $order->total_order = $row->total;
            $order->total_est_order = $row->total * 0.7;
            $order->comment_order = $row->comment;

            //$row->shipping_delivery_date
            //$order->from_delivery = $row->;
            $order->to_delivery = $row->shipping_city;
            $order->address_delivery = $row->shipping_address_1;
            //$order->time_delivery = $row->shipping_time_from . ' - ' . $row->shipping_time_to;
            $order->pickup_delivery = $row->shipping_address_2;

            $order->fio_delivery = $row->firstname . ' ' . $row->lastname;
            $order->phone_delivery = $row->telephone;

            $sql_product = 'select * from ' . $prefix3 . '_order_product where order_id='.$row->order_id;
            $product_rows = DB::connection('mysql_import')->select($sql_product);
            foreach ($product_rows as $product_row) {
                $product = Products::firstOrCreate(array('idfkorder' => $order->id, 'id_order_product' => $product_row->order_product_id));

                $product->title = $product_row->name;
                $product->model = $product_row->model;
                $product->price = $product_row->price;
                $product->quantity = $product_row->quantity;
                $product->total = $product_row->total;

                $product->save();
            }

            unset($product_rows);

            $order->save();
        }
        unset($rows);

    }
}
