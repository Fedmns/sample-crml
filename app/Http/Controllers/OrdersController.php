<?php

namespace App\Http\Controllers;

use Spatie\Valuestore\Valuestore;
use Illuminate\Http\Request;
use App\Products;

class OrdersController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $settings = Valuestore::make(storage_path('app/data.json'));

        return view('orders', ['settings' => $settings]);
    }

    public function OrderProducts($id_order) {
        $products = Products::where('idfkorder', $id_order)
            ->orderBy('id', 'asc')
            ->get();

        return $products;
    }
}
