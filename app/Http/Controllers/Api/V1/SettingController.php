<?php

namespace App\Http\Controllers\Api\V1;

use App\Shopsetting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SettingController extends Controller
{
/*
    public function __construct()
    {
        $this->middleware('auth');
    }
*/

    public function index()
    {
        return Shopsetting::all();
    }

    public function show($id)
    {
        return Shopsetting::findOrFail($id);
    }

    public function update(Request $request, $id)
    {
        $shopsetting = Shopsetting::findOrFail($id);
        $shopsetting->update($request->all());

        return $shopsetting;
    }

    public function store(Request $request)
    {
        $shopsetting = Shopsetting::create($request->all());
        return $shopsetting;
    }

    public function destroy($id)
    {
        $shopsetting = Shopsetting::findOrFail($id);
        $shopsetting->delete();
        return '';
    }
}

