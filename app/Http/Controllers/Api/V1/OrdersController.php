<?php

namespace App\Http\Controllers\Api\V1;

use App\Orders;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OrdersController extends Controller
{

    public function __construct()
    {
        //$this->middleware('auth');
    }


    public function index()
    {
        return Orders::all();
    }

    public function show($id)
    {
        return Orders::findOrFail($id);
    }

    public function update(Request $request, $id)
    {
        $orders = Orders::findOrFail($id);
        $orders->update($request->all());

        return $orders;
    }

    public function store(Request $request)
    {
        $orders = Orders::create($request->all());
        return $orders;
    }

    public function destroy($id)
    {
        $orders = Orders::findOrFail($id);
        $orders->delete();
        return '';
    }
}

