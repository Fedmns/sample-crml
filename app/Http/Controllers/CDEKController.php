<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use App\Orders;
use App\Products;
use App\Shopsetting;
use Spatie\Valuestore\Valuestore;
use Illuminate\Http\Request;
use GuzzleHttp\Client;

class CDEKController extends Controller
{

    private $token_type;
    private $access_token;
    private $jti;

    private $settings;
    private $client;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

        $this->client = new Client();

        $this->settings = Valuestore::make(storage_path('app/data.json'));
        $this->Auth();
    }

    public function Index()
    {

    }

    public function Test() {

        $id = 1;
        $order = Orders::findOrFail($id);

        if ($order->type_delivery == 1) { //склад-дверь СДЕК
            $tariff_code = 137;
        }
        if ($order->type_delivery == 2) { // склад-склад СДЕК
            $tariff_code = 136;
        }

        $number = $order->number_order.'_'.$order->codefkshop;
//'.$order->.'
        $data =
            '   
 {
    "type" : 1,
    "number" : "'.$number.'",
    "comment" : "ТЕСТОВЫЙ ЗАКАЗ / '.$order->comment_delivery.'",
    "from_location" : {
        "code" : "44",
        "fias_guid" : "",
        "postal_code" : "",
        "longitude" : "",
        "latitude" : "",
        "country_code" : "",
        "region" : "",
        "sub_region" : "",
        "city" : "Москва",
        "kladr_code" : "",
        "address" : "-, -"
    },
    "to_location" : {
        "code" : "44",
        "fias_guid" : "",
        "postal_code" : "",
        "longitude" : "",
        "latitude" : "",
        "country_code" : "",
        "region" : "",
        "sub_region" : "",
        "city" : "Москва",
        "kladr_code" : "",
        "address" : "ул. Ленина, 1"
    },
    "packages" : [
        {
        "number" : "001",
        "comment" : "'.$order->p_note.'",
        "weight" : '.$order->p1.',        
        "height" : '.$order->p4.',
        "length" : '.$order->p2.',
        "width" : '.$order->p3.',        
        "items" : [ {
            "ware_key" : "00055",
            "payment" : {
                "value" : 3000
            },
            "name" : "Товар",
            "cost" : 305,
            "amount" : 1,
            "weight" : 100,
            "url" : "www.item.ru"
        }
]
    },
        {
        "number" : "002",
        "comment" : "'.$order->p_note.'",
        "weight" : '.$order->p1.',        
        "height" : '.$order->p4.',
        "length" : '.$order->p2.',
        "width" : '.$order->p3.',        
        "items" : [
        {
            "ware_key" : "00056",
            "payment" : {
                "value" : 3009
            },
            "name" : "Товар",
            "cost" : 205,
            "amount" : 3,
            "weight" : 100,
            "url" : "www.item.ru"
        }]
    }],
    "recipient" : {
        "name" : "'.$order->fio_delivery.'",
        "phones" : [ {
        "number" : "'.$order->phone_delivery.'"
    } ]
    },
    "sender" : {
        "name" : "Магазин"
    },
    "tariff_code" : '.$tariff_code.'
}';


        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.cdek.ru/v2/orders",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 200,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS =>$data,
            CURLOPT_HTTPHEADER => array("Content-Type: application/json", "Authorization: ".$this->token_type.$this->access_token)
        ));

        $response = curl_exec($curl);
        curl_close($curl);

        $response = json_decode($response);
        echo $response->entity->uuid;
    }

    private function Auth() {

        $response = $this->client->request('POST', 'https://api.cdek.ru/v2/oauth/token?', [
                'form_params' => ['grant_type' => 'client_credentials',
                    'client_id' => $this->settings->get('client_id'),
                    'client_secret' => $this->settings->get('client_secret')]]
        );

        $obj_response = json_decode($response->getBody());
        $this->token_type = 'Bearer ';
        $this->access_token = $obj_response->access_token;

    }

    public function ExportCDEK(Request $request) {
        foreach ($request->IDs as $id) {
            $this->export_order($id);
        }

    }

    private function export_order($id) {

        $order = Orders::findOrFail($id);

        if ($order->type_delivery == 1) { //склад-дверь СДЕК
            $tariff_code = 137;
        }
        if ($order->type_delivery == 2) { // склад-склад СДЕК
            $tariff_code = 136;
        }
        $number = $order->number_order.'_'.$order->codefkshop;
        $to_code = $this->getCityID($order->to_delivery);

        if ((($order->p11) > 0) && (($order->p12) > 0) && (($order->p13) > 0) && (($order->p14) > 0)) {
            $p_count = 2;
        } else {
            $p_count = 1;
        }

        if ($p_count == 1) {
            $str_package =
                '"packages" : [
                    {
                        "number" : "001",
                        "comment" : "коментарий",
                        "weight" : '.$order->p1.',        
                        "height" : '.$order->p4.',
                        "length" : '.$order->p2.',
                        "width" : '.$order->p3.',        
                        "items" : [';

            $products = Products::where('idfkorder', $order->id)
                ->orderBy('id', 'asc')
                ->get();
            $str_product ='';
            $i = 0;
            $first= true;
            foreach ($products as $product) {
                $i = $i+1;

                $str = '
                         {
                            "ware_key" : "'.$i.'-'.$product->model.'",
                            "payment" : {
                                "value" : '.$product->price.'
                            },
                            "name" : "'.$product->title.'",
                            "cost" : '.$product->price.',
                            "amount" : '.$product->quantity.',
                            "weight" : 100,
                            "url" : "www.item.ru"
                        }';
                //\Log::info('data: '.$str);
                if ($first) {
                    $str_product = $str;
                    $first = false;
                } else {
                    $str_product = $str_product.' , '.$str;
                }
            }

            unset($product);

            $str_product = $str_product.
                        ']
                    
                    }],
                ';

            $str_package = $str_package.$str_product;

        } else {


            $products = Products::where('idfkorder', $order->id)
                ->orderBy('id', 'asc')
                ->get();

            $i = 0;
            $first= true;
            $first_package2= true;
            foreach ($products as $product) {
                $i = $i + 1;

                if ($first) {
                    $first = false;

                    $str_package1 =
                        '{
                        "number" : "001",
                        "comment" : "коментарий",
                        "weight" : ' . $order->p1 . ',        
                        "height" : ' . $order->p4 . ',
                        "length" : ' . $order->p2 . ',
                        "width" : ' . $order->p3 . ',        
                        "items" : [';

                    $str = '
                         {
                             "ware_key" : "' . $i . '-' . $product->model . '",
                            "payment" : {
                             "value" : ' . $product->price . '
                            },
                            "name" : "' . $product->title . '",
                            "cost" : ' . $product->price . ',
                            "amount" : ' . $product->quantity . ',
                            "weight" : 100,
                            "url" : "www.item.ru"
                        }]';

                    $str_package1 = $str_package1 . $str . '}';

                } else {

                    if ($first_package2) {
                        $first_package2 = false;
                        $str_package2 =
                            '{
                            "number" : "002",
                            "comment" : "коментарий",
                            "weight" : ' . $order->p11 . ',        
                            "height" : ' . $order->p14 . ',
                            "length" : ' . $order->p12 . ',
                            "width" : ' . $order->p13 . ',        
                            "items" : [';

                        $str = '
                             {
                                 "ware_key" : "' . $i . '-' . $product->model . '",
                                "payment" : {
                                 "value" : ' . $product->price . '
                                },
                                "name" : "' . $product->title . '",
                                "cost" : ' . $product->price . ',
                                "amount" : ' . $product->quantity . ',
                                "weight" : 100,
                                "url" : "www.item.ru"
                            }';
                        $str_product = $str;
                    } else {

                        $str = '
                             {
                                 "ware_key" : "' . $i . '-' . $product->model . '",
                                "payment" : {
                                 "value" : ' . $product->price . '
                                },
                                "name" : "' . $product->title . '",
                                "cost" : ' . $product->price . ',
                                "amount" : ' . $product->quantity . ',
                                "weight" : 100,
                                "url" : "www.item.ru"
                            }';

                        $str_product = $str_product.' , '.$str;

                    }
                }
            }

            $str_product = $str_product.']';

            $str_package2 = $str_package2 . $str_product . '}';

            unset($product);

            $str_package = '"packages" : ['.$str_package1.','.$str_package2.'],';

            // \Log::info('data: '.$str_package);

        }

//'.$order->.'
        $data =
            '   
 {
    "type" : 1,
    "number" : "'.$number.'",
    "comment" : "ТЕСТОВЫЙ ЗАКАЗ / '.$order->comment_delivery.'",
    "from_location" : {
        "code" : "44",
        "fias_guid" : "",
        "postal_code" : "",
        "longitude" : "",
        "latitude" : "",
        "country_code" : "",
        "region" : "",
        "sub_region" : "",
        "city" : "Москва",
        "kladr_code" : "",
        "address" : "-, -"
    },
    "to_location" : {
        "code" : "'.$to_code.'",
        "fias_guid" : "",
        "postal_code" : "",
        "longitude" : "",
        "latitude" : "",
        "country_code" : "",
        "region" : "",
        "sub_region" : "",
        "city" : "",
        "kladr_code" : "",
        "address" : "ул. Ленина, 1"
    },
    '.$str_package.'
    "recipient" : {
        "name" : "'.$order->fio_delivery.'",
        "phones" : [ {
        "number" : "'.$order->phone_delivery.'"
    } ]
    },
    "sender" : {
        "name" : "Магазин"
    },
    "tariff_code" : '.$tariff_code.'
}';


        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.cdek.ru/v2/orders",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 200,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS =>$data,
            CURLOPT_HTTPHEADER => array("Content-Type: application/json", "Authorization: ".$this->token_type.$this->access_token)
        ));

        $response = curl_exec($curl);
        curl_close($curl);
        \Log::info('data: '.$response);
        $response = json_decode($response);

        $this->updateUUIDAfterExportCDEK($response->entity->uuid, $order->id);
        
    }

    private function getCityID($name) {

        $res = DB::select('SELECT id FROM oc_cdek_city where cityName = ?', [$name]);

        return $res[0]->id;
    }

    private function getUUIDOrder($id) {

        $res = DB::select('SELECT uuid FROM orders where id = ?', [$id]);

        return $res[0]->uuid;
    }

    private function updateUUIDAfterExportCDEK($uuid, $order_id) {

        DB::table('orders')
            ->where('id', $order_id)
            ->update(['uuid' => $uuid, 'status_delivery' => 'Выгружен']);

        return '';
    }

    public function BuildInvoice($order_id) {

        $uuid = $this->getUUIDOrder($order_id);
        $data ='{
            "orders": [
                {
                "order_uuid": "'.$uuid.'",
                "copy_count": 2
                }
            ]
        }';

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.cdek.ru/v2/print/orders",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 200,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS =>$data,
            CURLOPT_HTTPHEADER => array("Content-Type: application/json", "Authorization: ".$this->token_type.$this->access_token)
        ));

        $response = curl_exec($curl);
        curl_close($curl);

        return $response;
        $response = json_decode($response);

    }

}

