<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Shopsetting extends Model
{
    protected $fillable = ['code', 'host', 'port', 'db_name', 'user_name', 'password', 'prefix', 'type'];
}
