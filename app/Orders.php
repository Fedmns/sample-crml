<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Orders extends Model
{
    protected $fillable = ['codefkshop', 'number_order', 'date_order', 'status_order',
        'total_order', 'total_est_order', 'comment_order', 'date_delivery', 'time_delivery', 'from_delivery',
        'to_delivery', 'address_delivery', 'pickup_delivery', 'fio_delivery', 'phone_delivery', 'status_delivery', 'comment_delivery',
        'type_delivery', 'p1', 'p2', 'p3', 'p4', 'p11', 'p12', 'p13', 'p14'
];
// ,'type_delivery', 'p1', 'p2', 'p3', 'p4', 'p11', 'p12', 'p13', 'p14', 'p_note'
}
