@extends('layouts.app')

@section('content')

<div class="panel-heading">дата обновлени {{ $settings->get('update_orders') }}</div>
<div class="panel-body table-responsive">

    <router-view name="OrdersIndex"></router-view>
    <router-view></router-view>
</div>

@endsection
