@extends('layouts.app')

@section('content')
<div class="panel-heading">Настройки магазинов</div>

<div class="panel-body table-responsive">
    <router-view name="ShopsettingIndex"></router-view>
    <router-view></router-view>
</div>
@endsection
