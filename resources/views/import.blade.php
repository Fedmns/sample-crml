@extends('layouts.app')

@section('content')
    <div class="panel-heading">Импорт заказов</div>
    <div class="panel-body table-responsive">
        <a href="#"
           class="btn btn-success"
           onclick="ImportOrders()">
            Импортировать
        </a>
    </div>

@endsection


<script>
    function ImportOrders() {
        axios.get('/admin/public/importorders')
                .then(function (resp) {
                    console.log(resp);
                    alert("Загрузка завершена");
                })
                .catch(function (resp) {
                    console.log(resp);
                    alert("Не удалось загрузить заказы");
                });

    }
</script>