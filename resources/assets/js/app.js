
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
import VueRouter from 'vue-router';

window.Vue.use(VueRouter);

import ShopsettingIndex from './components/shopsetting/ShopsettingIndex.vue';
import ShopsettingCreate from './components/shopsetting/ShopsettingCreate.vue';
import ShopsettingEdit from './components/shopsetting/ShopsettingEdit.vue';

import OrdersIndex from './components/orders/OrdersIndex.vue';
import OrdersCreate from './components/orders/OrdersCreate.vue';
import OrdersEdit from './components/orders/OrdersEdit.vue';

const routes = [

    {
        path: '/orders/view',
        components: {
            OrdersIndex: OrdersIndex
        }
    },

    {path: '/orders/create', component: OrdersCreate, name: 'createOrders'},
    
    {path: '/orders/edit/:id', component: OrdersEdit, name: 'editOrders'},
    {

        path: '/shopsetting/view',
        components: {
            ShopsettingIndex: ShopsettingIndex
        }
    },
    {path: '/shopsetting/create', component: ShopsettingCreate, name: 'createShopsetting'},
    {path: '/shopsetting/edit/:id', component: ShopsettingEdit, name: 'editShopsetting'}

]

const router = new VueRouter({ routes })

const app = new Vue({ router }).$mount('#app')