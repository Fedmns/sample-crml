<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('codefkshop', 30);
            $table->string('number_order', 50)->nullable();
            
            $table->date('date_order')->nullable();
            $table->string('status_order', 50)->nullable();
            $table->decimal('total_order', 15, 4)->nullable();
            $table->decimal('total_est_order', 15, 4)->nullable();
            $table->text('comment_order', 255)->nullable();

            $table->date('date_delivery')->default('1990.01.01');
            $table->string('from_delivery', 50)->nullable();
            $table->string('to_delivery', 50)->nullable();
            $table->string('address_delivery', 100)->nullable();
            $table->string('time_delivery', 50)->nullable();

            $table->string('pickup_delivery', 100)->nullable();
            $table->string('fio_delivery', 50)->nullable();
            $table->string('phone_delivery', 50)->nullable();
            $table->string('status_delivery', 50)->default('Не указан');
            $table->string('comment_delivery', 255)->nullable();
            $table->integer('type_delivery')->default(1);
            $table->smallInteger('p1')->nullable();
            $table->smallInteger('p2')->nullable();
            $table->smallInteger('p3')->nullable();
            $table->smallInteger('p4')->nullable();
            $table->smallInteger('p11')->nullable();
            $table->smallInteger('p12')->nullable();
            $table->smallInteger('p13')->nullable();
            $table->smallInteger('p14')->nullable();
            $table->string('p_note', 100)->nullable();
            $table->string('uuid', 100)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
