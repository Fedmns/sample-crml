<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShopsettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shopsettings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code')->nullable();
            $table->string('host')->nullable();
            $table->integer('port')->nullable();
            $table->string('db_name')->nullable();
            $table->string('user_name')->nullable();
            $table->string('password')->nullable();
            $table->string('prefix', 10)->nullable();
            $table->enum('type', array('opencart_2', 'opencart_3'))->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shopsettings');
    }
}
