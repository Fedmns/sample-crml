<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Spatie\Valuestore\Valuestore;

Route::get('/', function () {
    return view('welcome');
});


Auth::routes();

Route::get('/import', 'ImportOrdersController@index')->name('import');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/shopsetting', 'SettingController@index')->name('shopsetting');
Route::get('/orders', 'OrdersController@index')->name('orders');

Route::get('/importorders', 'ImportOrdersController@ImportOrders')->name('importorders');
Route::get('/order/products/{id}', 'OrdersController@OrderProducts')->name('orderproducts');

Route::get('/test', 'CDEKController@test')->name('test');

Route::get('/sendSD', 'CDEKController@ExportCDEK')->name('exportcdek');
Route::get('/invoice/{id}', 'CDEKController@BuildInvoice')->name('invoice');

// ДЛЯ передеачи параметра в шаблон
$settings = Valuestore::make(storage_path('app/data.json'));

use Illuminate\Http\Request;

Route::get('/sendGR', function (Request $request) {
    $settings = Valuestore::make(storage_path('app/data.json'));
    $settings->put('param', $request->all());
});

/*
//use Illuminate\Support\Facades\DB;

$config = Config::get('database.connections.mysql_import');
$config['database'] = "adwisor_db";
$config['username'] = "root";
$config['password'] = "";
config()->set('database.connections.mysql_import', $config);

$rows = DB::connection('mysql_import')->select('select * from sw_product limit 1');
foreach($rows as $row) {
    print_r($row->model);
}
*/
